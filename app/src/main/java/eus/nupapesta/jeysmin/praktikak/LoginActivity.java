package eus.nupapesta.jeysmin.praktikak;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.greenrobot.greendao.database.Database;
import java.util.Date;

import eus.nupapesta.jeysmin.praktikak.models.DaoMaster;
import eus.nupapesta.jeysmin.praktikak.models.DaoSession;
import eus.nupapesta.jeysmin.praktikak.models.Log;
import eus.nupapesta.jeysmin.praktikak.models.LogDao;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        EditText emailtext = (EditText) this.findViewById(R.id.emailtext);
        EditText passtext = (EditText) this.findViewById(R.id.passtext);
        Button loginbut = (Button) this.findViewById(R.id.loginbut);
        Button logoutbut = (Button) this.findViewById(R.id.logoutbut);
        TextView loginmessage = (TextView) this.findViewById(R.id.loginmessage);
        SharedPreferences pref = getSharedPreferences("user",0);
        String logemail = pref.getString("email","");
        if (!logemail.equals("")){
            loginmessage.append(" "+logemail);
            emailtext.setVisibility(View.GONE);
            passtext.setVisibility(View.GONE);
            loginbut.setVisibility(View.GONE);
        }else{
            loginmessage.setVisibility(View.GONE);
            logoutbut.setVisibility(View.GONE);
        }

    }
    public void login (View view){
        EditText emailtext = (EditText) this.findViewById(R.id.emailtext);
        EditText passtext = (EditText) this.findViewById(R.id.passtext);
        String emailstring = emailtext.getText().toString();
        String passstring = passtext.getText().toString();
        String message = "";
        if (!emailstring.equals("")){
            if (!passstring.equals("")){
                if (passstring.equals("dscr")){
                    SharedPreferences pref = getSharedPreferences("user",0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("email",emailstring);
                    editor.commit();

                    DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper (this,"dscr"); // "dscr" es el nombre de BD.
                    Database database = helper.getWritableDb();
                    DaoSession daoSession = new DaoMaster(database).newSession();
                    LogDao logDao = daoSession.getLogDao();
                    Log user = new Log();
                    user.setEmail(emailstring);
                    user.setAccess(new Date());
                    logDao.insert(user);
                    //List logs= logDao.queryBuilder().where(LogDao.Properties.Email.eq(emailstring)).list();

                    message = "Ongi Etorri "+emailstring+" !";
                    Toast.makeText(this,message,Toast.LENGTH_LONG).show();
                    finish();
                }else{
                    message = "Pasahitz okerra!";
                }
            }else { message = "Pasahitza eremua derrigorrezkoa da.";}
        }else{ message = "Email eremua derrigorrezkoa da.";}
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }
    public void exit (View view){
        SharedPreferences pref = getSharedPreferences("user",0);
        pref.edit().remove("email").commit();
        finish();
    }
}

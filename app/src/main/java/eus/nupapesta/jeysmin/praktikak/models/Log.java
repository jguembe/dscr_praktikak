package eus.nupapesta.jeysmin.praktikak.models;

import org.greenrobot.greendao.annotation.Entity;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.NotNull;

@Entity
public class Log {
    @NotNull
    private String email;
    @NotNull
    private Date access;
    @Generated(hash = 1374070749)
    public Log(@NotNull String email, @NotNull Date access) {
        this.email = email;
        this.access = access;
    }
    @Generated(hash = 1364647056)
    public Log() {
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public Date getAccess() {
        return this.access;
    }
    public void setAccess(Date access) {
        this.access = access;
    }
}

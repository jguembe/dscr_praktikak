package eus.nupapesta.jeysmin.praktikak;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import java.io.UnsupportedEncodingException;
import cz.msebera.android.httpclient.entity.StringEntity;
import eus.nupapesta.jeysmin.praktikak.Handlers.WelcomeResponseHandler;
import eus.nupapesta.jeysmin.praktikak.models.Name;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void login (View view){
        Intent loginintent = new Intent ( this , LoginActivity.class);
        startActivity(loginintent);
    }
    public void register(View view) {
        Intent registerintent = new Intent(this, RegisterActivity.class);
        startActivity(registerintent);
    }
    public void log(View view){
        Intent logintent = new Intent (this, LogActivity.class);
        startActivity(logintent);
    }
    public void welcome (View view) throws UnsupportedEncodingException {
        Name name = new Name("Fran");
        Gson gson = new Gson ();
        String namejson = gson.toJson(name);
        AsyncHttpClient client = new AsyncHttpClient();
        client.post (this, "http://api.messenger.tatai.es/welcome",
                new StringEntity(namejson), "application/json", new WelcomeResponseHandler(this));
    }
}

package eus.nupapesta.jeysmin.praktikak;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import org.greenrobot.greendao.database.Database;
import java.util.List;

import eus.nupapesta.jeysmin.praktikak.Adapters.AccessAdapter;
import eus.nupapesta.jeysmin.praktikak.models.DaoMaster;
import eus.nupapesta.jeysmin.praktikak.models.DaoSession;
import eus.nupapesta.jeysmin.praktikak.models.LogDao;

public class LogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper (this,"dscr",null); // "dscr" es el nombre de BD.
        Database database = helper.getReadableDb();
        DaoSession daoSession = new DaoMaster(database).newSession();
        LogDao logDao = daoSession.getLogDao();
        List logs= logDao.queryBuilder().orderDesc(LogDao.Properties.Access).list();

        AccessAdapter adapter = new AccessAdapter(logs);
        ListView accesos=(ListView) findViewById(R.id.accesslistview);
        accesos.setAdapter(adapter);
    }
}

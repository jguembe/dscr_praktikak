package eus.nupapesta.jeysmin.praktikak.Handlers;

import android.content.Context;
import android.widget.Toast;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import java.io.UnsupportedEncodingException;
import cz.msebera.android.httpclient.Header;
import eus.nupapesta.jeysmin.praktikak.models.DescriptionMessage;

public class WelcomeResponseHandler extends AsyncHttpResponseHandler {
    private Context context;
    public WelcomeResponseHandler(Context context){
        this.context = context;
    }
    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        String msg_json = null;
        try {
            msg_json = new String(responseBody, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        DescriptionMessage message = gson.fromJson(msg_json,DescriptionMessage.class);
        Toast.makeText(this.context,message.getMessage(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        String msg_json = null;
        try {
            msg_json = new String(responseBody, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        DescriptionMessage message = gson.fromJson(msg_json,DescriptionMessage.class);
        Toast.makeText(this.context,message.getMessage(),Toast.LENGTH_LONG).show();
    }
}

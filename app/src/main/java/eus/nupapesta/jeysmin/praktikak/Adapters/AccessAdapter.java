package eus.nupapesta.jeysmin.praktikak.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import eus.nupapesta.jeysmin.praktikak.R;
import eus.nupapesta.jeysmin.praktikak.models.DaoMaster;
import eus.nupapesta.jeysmin.praktikak.models.Log;

public class AccessAdapter extends BaseAdapter {
    private List lista;

    public AccessAdapter(List lista){
        this.lista = lista;
    }
    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int position) {
        return this.lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log log = (Log)this.getItem(position);
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_two_text, parent, false);
        ((TextView)convertView.findViewById(R.id.username)).setText(log.getEmail());
        ((TextView)convertView.findViewById(R.id.date)).setText(log.getAccess().toString());

        return convertView;
    }
}

package eus.nupapesta.jeysmin.praktikak;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void register(View view) {
        String  message ="";
        TextView emailtext = (TextView) this.findViewById(R.id.emailtext);
        TextView passtext = (TextView) this.findViewById(R.id.passtext);
        String emailstring = emailtext.getText().toString();
        String passstring = passtext.getText().toString();
        if (!emailstring.equals("")){
            if (!passstring.equals("")){
                message = "Zure kontua "+emailstring+" sortu da!";
            }else {message = "Pasahitza eremua ezin da hutsik utzi.";}
        }else {message = "E-mail eremua ezin da hutsik utzi.";}
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }
}

package eus.nupapesta.jeysmin.praktikak.models;


public class DescriptionMessage {
    private String message;

    public DescriptionMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
